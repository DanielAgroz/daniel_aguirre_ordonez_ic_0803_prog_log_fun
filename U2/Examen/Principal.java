public class Principal { // 1
	

//10
	private static void miMetodo(PruebaExamen OBJ){
		//11
	OBJ.operacionPrueba(2, PruebaExamen.operacionPrueba(10,2), PruebaExamen.operacionPrueba(2,3));

	}
//14
	private static void miMetodo(PruebaExamen OBJ, int x, int y, int z){
		//15
	OBJ.operacionPrueba( x, PruebaExamen.operacionPrueba(y,y), PruebaExamen.operacionPrueba(z,z));

	}

//2
	public static void main (String [] args){
//3
		 PruebaExamen pe;
//4 
		 pe = (x, y, z) -> System.out.println(x + y + z +"  <-- Suma de 3 numeros ");
		pe.operacionPrueba(10,11,12);
//5 	 
		System.out.println( PruebaExamen.operacionPrueba(10, 5)+" <- Resultado ");
//6 
		//System.out.println("Metodo sin parametro -->" PruebaExamen.mensajeHola());
		pe.mensajeHola();
//7
		//System.out.println("Metodo con parametro -->"+PruebaExamen.mensajeHola("HOLA SI FUNCIONO"));
		pe.mensajeHola("HOLA SI FUNCIONO");
//8
		 pe = (x, y, z) -> System.out.println( x + (y * z)+" <-- Resultado de la suma del primer arg ("+x+") con la multiplicacion del Segund arg ("+y+") * el tercero ("+z+")");
		pe.operacionPrueba(10,11,12);
//9
		// pe = (x, y, z) -> System.out.println();
		 pe.operacionPrueba(10, PruebaExamen.operacionPrueba(1,1), PruebaExamen.operacionPrueba(1,1));
//12 
		miMetodo(pe);
//13
		miMetodo(pe = (x,y,z) -> System.out.println(x + (y / z)+" <-- Resultado de la suma del primer arg ("+x+") con la division1 del Segund arg ("+y+") * el tercero ("+z+")"));
//16
		pe = (x, y, z) -> {
			float a = (float)y / z;
			 a = a + x;
			System.out.println(a+" <-- Resultado suma del primer arg ("+x+") con la division2 del Segund arg ("+y+") * el tercero ("+z+")");

		};
//17
	miMetodo(pe, 4, 6, 9);
	}
}