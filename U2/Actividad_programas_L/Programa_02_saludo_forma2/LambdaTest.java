
public class LambdaTest{
	
	public void miMetodo (FunctionTest parametro){        // Utilizacion de un metodo en la clase 
		
		parametro.saludar();
	}
	
	
	public static void main (String[] args){
		
		FunctionTest FT = () -> System.out.println("Hola mundo cruel sin parametros");  // implementacion de metodo abstracto "saludar" 
		
		
		LambdaTest objeto = new LambdaTest();  // creacion de un objeto
		objeto.miMetodo(FT);   //usando el obj con el metodo creado
		
	}
	
}