
public class LambdaTest{
	
	// implementando el metodo abstracto del otro .java "imprimeS", ejecucion sin la creacion de un objeto 
	
	public static void main (String[] args){
		
		Operaciones OPE = (num_1, num_2) -> System.out.println( num_1 + num_2+" <- Resultado de la suma de "+num_1+" + "+num_2);
		
		OPE.imprimeS(10,5);
		
		
		
	}
	
}