
public class LambdaTest{
	
	public static void main (String[] args){
		
		FunctionTest FT = () -> System.out.println("Hola mundo cruel");  // implementacion de metodo abstracto "saludar" 
		FT.saludar();  // Ejecutar el metodo
		
	}
	
}