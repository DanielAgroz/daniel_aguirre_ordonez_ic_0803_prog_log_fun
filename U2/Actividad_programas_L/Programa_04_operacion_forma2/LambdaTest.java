
public class LambdaTest{
	
	public void miMetodo(Operaciones OPE, int num_1, int num_2){
		
		OPE.imprimeS(num_1,num_2);
		
	}
	
	public static void main (String[] args){
		
		Operaciones OPE = (num_1, num_2) -> System.out.println( num_1 + num_2+" <- Resultado de la suma de "+num_1+" + "+num_2);
		
		LambdaTest objeto = new LambdaTest();   // implementacion utilizando un objeto y un metodo adicional 
		objeto.miMetodo(OPE, 10, 10);
		
		
	}
	
}