import java.util.List;

public class FP_Functional_Exercises{
	
	public static void main(String[] args){
		
		List<Integer> numbers = List.of(12, 9, 13, 4, 6, 2, 4, 12, 15);
		
		List<String> courses = List.of("Spring", "Spring Boot", "API", 
		"Microservices", "AWS", "PCF", "Azure", "Docker", "Keburnetes");
		
		
		System.out.println("\n 1.- Numeros impares ");
		print_Num_Impares(numbers);
		System.out.println("");
		
		System.out.println("\n 2.- Imprimir todos los Courses ");
		printAll_C(courses);
		System.out.println("");
		
		System.out.println("\n 3.- Imprimir los Courses con Spring");
		print_Solo_Sp(courses);
		System.out.println("");
		
		System.out.println("\n 4.- Imprimir los Courses con logitud <= 4");
		print_MAX_4(courses);
		System.out.println("");
		
			System.out.println("\n 5.- Imprimir los impares^3");
		print_impar_cubo(numbers);
		System.out.println("");
		
		System.out.println("\n 6.- Imprimir los Courses con su logitud");
		print_ALL_Course_N_Characters(courses);
		System.out.println("");
		
		
	}
	
	
	// METODOS PARA UTILIZAR
	
	public static void P(int number){ System.out.print(number + ", ");}
	
	public static void P(String course){ System.out.print(course + ", ");}
	
	public static void L(String course){ System.out.print(course + course.length() + ", ");}

	public static boolean TF (int number){ return (number % 2 != 0);}
	
	public static boolean TF (String course){ return (course.length() >= 4);}
	
	

	
  //1
  
private static void print_Num_Impares (List<Integer> numbers){

	numbers.stream()
	.filter(FP_Functional_Exercises::TF)
	.forEach(FP_Functional_Exercises::P);
	System.out.println("");

}
 // 2
private static void printAll_C(List<String> courses){

		courses.stream().forEach(FP_Functional_Exercises::P);
		System.out.println("");

	}

// 3
	
private static void print_Solo_Sp(List<String> courses){

	courses.stream().filter(course -> course.contains("Spring")).forEach(FP_Functional_Exercises::P);
	System.out.println("");

}

// 4

private static void print_MAX_4(List<String> courses){

	courses.stream().filter(FP_Functional_Exercises::TF).forEach(FP_Functional_Exercises::P);
	System.out.println("");

}

// 5

private static void print_impar_cubo(List<Integer> numbers){

	numbers.stream().filter(FP_Functional_Exercises::TF).map(number -> number * number * number ).forEach(FP_Functional_Exercises::P);
	System.out.println("");

}

//6

private static void print_ALL_Course_N_Characters(List<String> courses){

	courses.stream().forEach(FP_Functional_Exercises::L);
		System.out.println("");
}
	
	
}