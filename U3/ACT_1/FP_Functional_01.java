import java.util.List;

public class FP_Functional_01{
	
	public static void main(String[] args) {
		
		List<Integer> numbers = List.of(12, 9, 13, 4, 6, 2, 4, 12, 15);

		System.out.println("Using to --> [System.out::print] by default");
		printAll_F_T(numbers);
		System.out.println("\n printAll_F: ");
		printAll_F(numbers);
		System.out.println("\n printEven_F: ");
		printEven_F(numbers);
		System.out.println("\n printS_of_Even_F: ");
		printS_of_Even_F(numbers);
		System.out.println("");
	}

	private static void print(int number){

		System.out.print(number + ", ");

	}

	private static boolean isEven (int number){

		return (number % 2 == 0);

	}

	private static void printAll_F_T(List<Integer> numbers){

		numbers.stream()
		.forEach(System.out::print);
		System.out.println("");

	}

private static void printAll_F (List<Integer> numbers){

		numbers.stream()
		.forEach(FP_Functional_01::print);
		System.out.println("");

	}

private static void printEven_F (List<Integer> numbers){

	numbers.stream()
	.filter(FP_Functional_01::isEven)
	.forEach(FP_Functional_01::print);
	System.out.println("");

}

private static void printS_of_Even_F (List<Integer> numbers){

	numbers.stream()
	.filter(number -> number % 2 == 0)
	.map(number -> number * number)
	.forEach(FP_Functional_01::print);
	System.out.println("");

}

}